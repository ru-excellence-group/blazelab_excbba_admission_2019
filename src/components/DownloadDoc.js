import React, {Component} from 'react';
//import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
//import AnchorLink from 'react-anchor-link-smooth-scroll';

class DownloadDoc extends Component {
  render () {
    return (
      <div className="FullScreenBoxNormal" id="download">
        <div className="WrapperNormal BlueBG">

          {/*<div className="admission-box">
            <div id="admission-box-1">

              <h2 className="yellowText">ดาวน์โหลด</h2>
              <div className="btnGroup">
                <a
                  className="whiteBtn"
                  target="_blank"
                  rel="noopener noreferrer"
                  href="http://www.excbba.ru.ac.th/attachment_file/20190328/open-for-apply-excbba-2019-9.pdf"
                >
                  ประกาศรับสมัคร
                </a>

                <a
                  className="whiteBtn"
                  target="_blank"
                  rel="noopener noreferrer"
                  href="http://www.excbba.ru.ac.th/attachment_file/20190328/open-for-apply-excbba-2019-9.pdf"
                >
                  ประกาศรับสมัคร
                </a>
              </div>

            </div>
          </div> */}

          <div className="centeredContent">
            <h2 className="yellowText">ดาวน์โหลด</h2>
          </div>
          <div className="btnGroup">
            <a
              className="whiteBtn"
              target="_blank"
              rel="noopener noreferrer"
              href="http://www.excbba.ru.ac.th/attachment_file/20190328/open-for-apply-excbba-2019-9.pdf"
            >
              ประกาศรับสมัครอย่างเป็นทางการ
            </a>

            <a
              className="whiteBtn"
              target="_blank"
              rel="noopener noreferrer"
              href="http://www.excbba.ru.ac.th/admission/media/active-banner.png"
            >
              แบนเนอร์
            </a>

            <a
              className="whiteBtn"
              target="_blank"
              rel="noopener noreferrer"
              href="http://www.excbba.ru.ac.th/admission/media/active-poster.png"
            >
              โปสเตอร์
            </a>
            <a
              className="whiteBtn"
              target="_blank"
              rel="noopener noreferrer"
              href="http://www.excbba.ru.ac.th/admission/media/brosure-excbba-2019-1.pdf"
            >
              โบรชัวร์
            </a>
          </div>
        </div>
      </div>
    );
  }
}

export default DownloadDoc;
