import React, {Component} from 'react';
//import {BrowserRouter as Router, Route, Link} from 'react-router-dom';

class Schedule extends Component {
  render () {
    return (
      <div className="FullScreenBoxNormal" id="scheduleUl">
        <div className="WrapperNormal BlueBG">

          <div className="admission-box">
            <div id="admission-box-1">
              <h2 className="yellowText">กำหนดการ</h2>
              <ul className="whiteText ulPadding20 scheduleUlMobile">
                <li>รับสมัคร สัมภาษณ์ และขึ้นทะเบียน/ลงทะเบียน</li>
                <ul>
                  <li>วันนี้ - วันจันทร์ที่ 24 มิถุนายน 2562</li>
                </ul>
                <li>ปฐมนิเทศนักศึกษาใหม่</li>
                <ul>
                  <li>วันศุกร์ที่ 28 มิถุนายน 2562</li>
                </ul>
                <li>เปิดเรียน</li>
                <ul>
                  <li>วันจันทร์ที่ 1 กรกฏาคม 2562</li>
                </ul>
              </ul>

              <table
                className="excTable whiteText scheduleUlDesktop"
                id="scheduleTable"
              >
                <tbody>
                  <tr>
                    <th>ขั้นตอน</th>
                    <th>กำหนดการ</th>
                  </tr>
                  <tr>
                    <td>รับสมัคร สัมภาษณ์ และขึ้นทะเบียน/ลงทะเบียน</td>
                    <td>วันนี้ - วันจันทร์ที่ 24 มิถุนายน 2562</td>
                  </tr>
                  <tr>
                    <td>ปฐมนิเทศนักศึกษาใหม่</td>
                    <td>วันศุกร์ที่ 28 มิถุนายน 2562</td>
                  </tr>
                  <tr>
                    <td>เปิดเรียน</td>
                    <td>วันจันทร์ที่ 1 กรกฏาคม 2562</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>

          <div className="admission-box">
            <div id="admission-box-1">
              <h2 className="yellowText">ค่าธรรมเนียมการศึกษาตลอดหลักสูตร</h2>

              <ul className="whiteText ulPadding20 scheduleUlMobile">
                <li>งวดที่ 1 - 7</li>
                <ul>
                  <li>จำนวนเงิน งวดละ 19,900 บาท</li>
                </ul>
                <li>งวดที่ 8</li>
                <ul>
                  <li>จำนวนเงิน 12,800 บาท</li>
                </ul>
                <b>รวม 152,100 บาท</b>
              </ul>

              <table
                className="excTable whiteText scheduleUlDesktop"
                id="tutionTable"
              >
                <tbody>
                  <tr>
                    <th />
                    <th />
                    <th>จำนวนเงิน (บาท)</th>
                  </tr>
                  <tr>
                    <td>งวดที่ 1</td>
                    <td>ภาค 2/2562</td>
                    <td>19,900</td>
                  </tr>
                  <tr>
                    <td>งวดที่ 2</td>
                    <td>ภาค ฤดูร้อน/2562</td>
                    <td>19,900</td>
                  </tr>
                  <tr>
                    <td>งวดที่ 3</td>
                    <td>ภาค 1/2563</td>
                    <td>19,900</td>
                  </tr>
                  <tr>
                    <td>งวดที่ 4</td>
                    <td>ภาค 2/2563</td>
                    <td>19,900</td>
                  </tr>
                  <tr>
                    <td>งวดที่ 5</td>
                    <td>ภาค ฤดูร้อน/2563</td>
                    <td>19,900</td>
                  </tr>
                  <tr>
                    <td>งวดที่ 6</td>
                    <td>ภาค 1/2564</td>
                    <td>19,900</td>
                  </tr>
                  <tr>
                    <td>งวดที่ 7</td>
                    <td>ภาค 2/2564</td>
                    <td>19,900</td>
                  </tr>
                  <tr>
                    <td>งวดที่ 8</td>
                    <td>ภาค ฤดูร้อน/2564</td>
                    <td>12,800</td>
                  </tr>
                  <tr>
                    <th />
                    <th />
                    <th>รวม 152,100</th>
                  </tr>
                </tbody>
              </table>

              <ul className="whiteText ulPadding20 noStyle">
                <li>ค่าธรรมเนียมครอบคลุมรายการต่อไปนี้</li>
                <ul>
                  <li>ค่าขึ้นทะเบียนเป็นนักศึกษา และค่าลงทะเบียนเรียน</li>
                  <li>ตำราหรือเอกสารประกอบคำบรรยาย</li>
                  <li>เสื้อโปโล 1 ตัว</li>
                  <li>กิจกรรมปฐมนิเทศ</li>
                  <li>ค่าใช้จ่ายในการสัมมนาทางวิชาการ</li>
                  <li>ปัจฉิมนิเทศ ณ มหาวิทยาลัยรามคำแหง (หัวหมาก)</li>
                </ul>
              </ul>

            </div>
          </div>

        </div>
      </div>
    );
  }
}

export default Schedule;
