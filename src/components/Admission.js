import React, {Component} from 'react';
//import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import AnchorLink from 'react-anchor-link-smooth-scroll';

class Admission extends Component {
  render () {
    return (
      <div className="FullScreenBoxNormal" id="Admission">
        <div className="WrapperNormal">

          <div className="admission-box">
            <div id="">
              <h2 className="blueText">คุณสมบัติผู้สมัคร</h2>
              <ul className="">
                <li>
                  สำเร็จการศึกษาระดับมัธยมศึกษาตอนปลาย (ม.6) หรือเทียบเท่า
                </li>
                <li>
                  สำเร็จการศึกษาระดับอนุปริญญา ได้แก่ ระดับ ปวส. หรือเทียบเท่า
                </li>
                <li>ผู้กำลังศึกษาระดับปริญญาตรี</li>
                <li>ผู้จบการศึกษาระดับปริญญาตรีทุกสาขา</li>
                <li>
                  เป็นข้าราชการซึ่งมีตำแหน่งและเงินเดือน ตั้งแต่ระดับปฎิบัติงานขึ้นไป
                  และเป็นผู้สำเร็จการศึกษาตามหลักสูตรภาคบังคับการศึกษาขั้นพื้นฐาน (มัธยมศึกษาตอนต้น) หรือเทียบเท่า
                </li>
                <li>
                  เป็นหรือเคยเป็นข้าราชการ พนักงานรัฐวิสาหกิจ หรือลูกจ้างของรัฐซึ่งได้ปฏิบัติงานมานานแล้ว
                  รวมกันไม่น้อยกว่า 5 ปี และจบหลักสูตรมัธยมศึกษาตอนต้น
                </li>
                <li>
                  เป็นหรือเคยเป็นสมาชิกรัฐสภา สมาชิกสภากรุงเทพมหานคร สมาชิกสภาจังหวัด สมาชิกสภาองค์การบริหารจังหวัด
                  สมาชิกสภาเขต สมาชิกสภาเมืองพัทยา สมาชิกเทศบาล สมาชิกตำบล สมชิกสภาองตค์การบริหารส่วนตำบล
                  หรือกรรมการสุขาภิบาล หรือกำนันผู้ใหญ่บ้าน ทั้งนี้ต้องจบหลักสูตรมัธยมศึกษาตอนต้น หรือเทียบเท่า
                </li>
              </ul>
            </div>
          </div>

          <div className="admission-box-small">
            <div id="" className="MidBlueBG shadow-lg">
              <h2 className="yellowText marginTopZero">การสมัคร</h2>
              <ul className="whiteText noStyle">
                <li>
                  สมัครด้วยตนเองได้ที่<br />
                  <AnchorLink className="whiteText" href="#office">
                    สำนักงานโครงการฯ
                  </AnchorLink>
                  <br />
                  พร้อมชำระค่าสมัคร 700 บาท<br />
                </li>
                <li>
                  หรือ
                </li>
                <li id="applyOnlineBtn">
                  <a
                    className="whiteText whiteBtnBlue"
                    target="_blank"
                    rel="noopener noreferrer"
                    href="http://www.oros.ru.ac.th/excbba/ent/app_online.jsp?owner_id=017"
                  >
                    สมัครออนไลน์
                  </a>
                </li>
              </ul>
            </div>
            <div id="" className="MidBlueBG shadow-lg">
              <h2 className="yellowText marginTopZero">หลักฐานการสมัคร</h2>
              <ul className="whiteText ">
                <li>
                  ใบสมัครกรอกข้อความครบถ้วน<br />ติดรูปถ่ายสีขนาด 2 นิ้ว 3 ใบ
                </li>
                <li>
                  สำเนาวุฒิการศึกษา 2 ฉบับ<br />
                  "วุฒิการศึกษาต้องระบุการจบหลักสูตร<br />
                  และวันที่สำเร็จการศึกษาเท่านั้น"
                  <ul>
                    <li>
                      กรณีมีการเปลี่ยนชื่อ - สกุล<br />
                      ต้องแนบสำเนาใบเปลี่ยนชื่อ-สกุล<br />
                      หรือใบสมรส หรือใบหย่า (แล้วแต่กรณี)<br />
                      ให้เท่าจำนวนวุฒิการศึกษา (ถ้ามี)
                    </li>
                    <li>
                      กรณีใช้วุฒิการศึกษาจากต่างประเทศ<br />
                      ให้ดำเนินการขอใบเทียบระดับความรู้ที่<br />
                      กระทรวงศึกษาธิการก่อนการสมัคร<br />
                      และถ่ายสำเนาใบเทียบความรู้เท่ากับ<br />
                      วุฒิการศึกษานั้นมาในวันสมัครด้วย​ (ถ้ามี)<br />
                    </li>
                  </ul>
                </li>
                <li>
                  สำเนาทะเบียนบ้าน<br />
                  ที่มีชื่อ - สกุล 2 ฉบับ
                </li>
                <li>
                  สำเนาบัตรประชาชน 4 ฉบับ
                </li>
                <li>
                  ใบรับรองแพทย์
                </li>
              </ul>
            </div>
            <div id="" className="MidBlueBG shadow-lg">
              <h2 className="yellowText marginTopZero">
                ขึ้นทะเบียน/ลงทะเบียน
              </h2>
              <ul className="whiteText noStyle">
                <li>
                  ผู้สมัครจะต้องลงทะเบียนเรียน<br />
                  ภายในช่วงเวลารับสมัคร
                </li>
                <li>
                  พร้อมชำระค่าธรรมเนียมการศึกษา<br />
                  งวดที่ 1 จำนวน 19,900 บาท<br />
                  โดยให้ชำระด้วยแคชเชียร์เช็ค<br />
                  สั่งจ่าย มหาวิทยาลัยรามคำแหง<br />
                  พร้อมเขียน ชื่อ - สกุล ด้านหลังเช็ค<br />
                </li>
              </ul>
            </div>
          </div>

          <div className="admission-box" id="transfer-details">
            <div id="admission-box-2" className="centered-box">
              <h2 className="blueText">
                หลักฐานเพิ่มเติม กรณีใช้สิทธิ์เทียบโอน
              </h2>
            </div>
          </div>

          <div className="admission-box-small">
            <div id="" className="MidBlueBG shadow-lg">
              <h2 className="yellowText marginTopZero">
                กรณีเทียบโอนระดับอนุปริญญา<br />หรือเทียบเท่าขึ้นไป
              </h2>
              <ul className="whiteText">
                <li>
                  สำเนาวุฒิการศึกษา 4 ฉบับ
                </li>
              </ul>
            </div>
            <div id="" className="MidBlueBG shadow-lg">
              <h2 className="yellowText marginTopZero">
                กรณีเทียบโอนจากสถาบันอื่น
              </h2>
              <ul className="whiteText ">
                <li>
                  Transcript ฉบับจริง 1 ฉบับ
                </li>
                <li>
                  คำอธิบายรายวิชา
                </li>
                <li>
                  ใบหมดสถานะภาพการเป็นนักศึกษา/ใบลาออก
                </li>
              </ul>
            </div>
            <div id="" className="MidBlueBG shadow-lg">
              <h2 className="yellowText marginTopZero">
                กรณีเทียบโอนรายวิชา<br />จากมหาวิทยาลัยรามคำแหง
              </h2>
              <ul className="whiteText">
                <li>
                  Transcript ฉบับจริง 1 ฉบับ
                </li>
                <li>
                  ใบหมดสถานะภาพการเป็นนักศึกษา/ใบลาออก
                </li>
              </ul>
            </div>
          </div>

        </div>
      </div>
    );
  }
}

export default Admission;
