import React, {Component} from 'react';
//import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import AnchorLink from 'react-anchor-link-smooth-scroll';
import TextLoop from 'react-text-loop';

class Review extends Component {
  render () {
    return (
      <div className="FullScreenBoxNormal" id="Review">
        <div className="WrapperNormal MidBlueBG">

          <div className="admission-box" id="text-review">
            <div id="admission-box-2" className="centered-box">
              <h2 className="yellowText">
                บริหารธุรกิจบัณฑิต สาขาวิชาการจัดการ<br />
                <br />
                เรียนวันจันทร์ - วันพฤหัสบดี<br />
                เวลา 17.30 - 21.30น.<br />
                ที่ มหาวิทยาลัยรามคำแหง หัวหมาก
              </h2>
            </div>
          </div>

          {/* <div className="admission-box" id="transfer-details">
            <div id="admission-box-2" className="centered-box">
              <h2 className="blueText">
                <TextLoop>
                  <span>
                    เรียนภาคค่ำ<br />
                    บรรยายสดทุกชั่วโมง
                  </span>
                  <span>
                    มีคะแนนเก็บแต่ละวิชา<br />
                    มีแผนการเรียนที่ชัดเจน
                  </span>
                  <span>
                    ตลาดแรงงานกว้าง<br />
                    ทั้งภาครัฐและภาคเอกชน
                  </span>
                </TextLoop>{' '}
              </h2>
            </div>
          </div> */}

          <div className="admission-box-small" id="review-box">
            <div id="" className="BlueBG shadow-lg">
              <h2 className="whiteText marginTopZero marginBottomZero textCenter">
                เรียนภาคค่ำ<br />
                บรรยายสดทุกชั่วโมง
              </h2>
            </div>
            <div id="" className="BlueBG shadow-lg">
              <h2 className="whiteText marginTopZero marginBottomZero textCenter">
                เรียนแบบ Block Course<br />
                มีคะแนนเก็บแต่ละวิชา<br />
                มีแผนการเรียนที่ชัดเจน
              </h2>
            </div>
            <div id="" className="BlueBG shadow-lg">
              <h2 className="whiteText marginTopZero marginBottomZero textCenter">
                ตลาดแรงงานกว้าง<br />
                ทั้งภาครัฐและภาคเอกชน
              </h2>
            </div>
          </div>

          <div className="admission-box" id="transfer-details">
            <div id="admission-box-2" className="centered-box">
              <h2 className="yellowText">
                อยากเป็น..{' '}<span id="steptextLoopReview" />
                <TextLoop
                  interval={2500}
                  springConfig={{stiffness: 180, damping: 8}}
                  children={[
                    'นักวิเคราะห์นโยบายและแผน',
                    'นักวิชาการบริหารงานทั่วไป',
                    'นักวิชาการการเงิน',
                    'นักวางแผนการผลิต',
                    'งานบริหารคลังสินค้า',
                    'งานด้านการตลาด',
                    'พนักงานธนาคาร',
                    'งานเลขานุการ',
                  ]}
                />
              </h2>
              <div id="applyNowBtnReview">
                <AnchorLink href="#Admission" className="blueText whiteBtnBlue">
                  สมัครเลย
                </AnchorLink>
              </div>
            </div>
          </div>

        </div>
      </div>
    );
  }
}

export default Review;
