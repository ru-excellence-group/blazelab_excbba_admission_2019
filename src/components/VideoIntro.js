import React, {Component} from 'react';
//import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import AnchorLink from 'react-anchor-link-smooth-scroll';

class VideoIntro extends Component {
  render () {
    return (
      <div className="FullScreenBox" id="Video">
        <div className="WrapperFluid WrapperFluidTop">
          <div className="centeredContent">
            <h1>Video</h1>
          </div>
          <AnchorLink className="nextButton" href="#Admission">
            ถัดไป
          </AnchorLink>
        </div>
      </div>
    );
  }
}

export default VideoIntro;
