import React, {Component} from 'react';
import {Link} from 'react-router-dom';
//import AnchorLink from 'react-anchor-link-smooth-scroll';
import TextLoop from 'react-text-loop';

class Review extends Component {
  render () {
    return (
      <div className="FullScreenBoxNormal" id="PrivacyPolicy">
        <div className="WrapperNormal MidBlueBG">

          <div className="admission-box" id="text-policy">
            <div id="admission-box-2" className="">
              <h2 className="yellowText">
                นโยบายความเป็นส่วนตัว<br />
              </h2>
              <p className="whiteText">
                โครงการธุรกิจบัณฑิต เพื่อความเป็นเลิศ ("เรา" “พวกเรา” หรือ "ของเรา")
                {' '}
                เคารพสิทธิความเป็นส่วนตัวของผู้เข้าเยี่ยมชม ผู้ใช้ ผู้รับบริการ และผู้ใช้งานอื่นๆทั้งหมด
                {' '}
                (ให้เรียกรวมกันว่า "ท่าน") เราจึงออกนโยบายฉบับนี้เพื่อเป็นการชี้แจงให้ท่านทราบและเข้าใจเกี่ยวกับการใช้ข้อมูลส่วนบุคคลของท่าน
                {' '}
                เราจำเป็นต้องใช้ข้อมูลของท่านเพื่อนำมาปรับปรุงให้มีคุณภาพยิ่งขึ้นและมีเนื้อหาตรงตามความต้องการของท่าน เมื่อท่านได้ใช้บริการของเรา เว็บไซต์ (“บริการ”)
                {' '}
                เราถือว่าท่านได้ตกลงตามนโยบายความเป็นส่วนตัวฉบับนี้แล้ว ทั้งนี้เราอาจแก้ไขเนื้อหาของนโยบายได้ตลอดเวลา
                {' '}
                เราจึงขอแนะนำให้ท่านอ่านและทำความเข้าใจนโยบายฉบับนี้อยู่เสมอๆ
              </p>
              <h2 className="yellowText">
                วัตถุประสงค์ในการเก็บข้อมูล
              </h2>
              <p className="whiteText">
                เว็บไซต์ของเราจะไม่เก็บรวบรวมข้อมูลที่สามารถระบุตัวตนของท่าน
                เว้นแต่ท่านจะสมัครใจให้ข้อมูลนั้นแก่เรา และเราจะนำข้อมูลส่วนบุคคลของท่านไปใช้เพื่อเฉพาะประโยชน์ในการปรับปรุงพัฒนาตัวเว็บไซต์ของเราให้สะดวกสบายและเหมาะสมแก่การใช้งานของท่านเอง
              </p>
              <h2 className="yellowText">
                การเก็บรักษาและการเปิดเผยข้อมูลส่วนบุคคล
              </h2>
              <p className="whiteText">
                เราพยายามอย่างเต็มที่เพื่อคุ้มครองข้อมูลความเป็นส่วนบุคคลของท่าน
                {' '}
                เราพัฒนาระบบความปลอดภัยอย่างต่อเนื่องเพื่อปกป้องข้อมูลส่วนบุคคลของท่านไม่ให้ตกไปสู่บุคคลภายนอก
                {' '}
                เราเพียงเก็บรักษาและใช้ข้อมูลส่วนบุคคลของท่านเพื่อพัฒนาระบบเว็บไซต์และพัฒนาบริการของเราให้ดีขึ้นเท่านั้น
                {' '}
                เราไม่มีจุดประสงค์หรือนโยบายในการส่งต่อ ขาย ให้ข้อมูลไปยังบริษัทหรือบุคคลอื่น
                {' '}
                อย่างไรก็ตามเว็บไซต์ของเราอาจมีลิงค์เชื่อมโยงไปสู่เว็บไซต์ภายนอก
                {' '}
                หรืออาจเป็นลิงค์จากเว็บไซต์ภายนอกที่เชื่อมโยงเข้ามายังเว็บไซต์ของเรา
                {' '}
                เราจึงขอแจ้งท่านว่าลิงค์เหล่านี้อยู่เหนือการควบคุมของเรา
                เราไม่สามารถควบคุมนโยบายหรือแนวทางการรักษาและใช้ข้อมูลส่วนบุคคลของเว็บไซต์อื่นเช่น Facebook หรือ Google ได้
                {' '}
                เราจึงขอแนะนำให้ท่านศึกษานโยบายและเงื่อนไข ข้อตกลงซึ่งเกี่ยวกับการใช้งานเว็บไซต์เหล่านั้นด้วย
              </p>
              <h2 className="yellowText">
                ข้อมูลที่เราเก็บรวบรวมโดยอัตโนมัติ และคุกกี้
              </h2>
              <p className="whiteText">
                เมื่อท่านเข้าใช้งานเว็บไซต์ของเรา เราอาจเก็บข้อมูลบางประเภทโดยอัตโนมัติ  ข้อมูลดังกล่าวได้แก่
                <li>วันและเวลาที่ท่านเข้าใช้งาน</li>
                <li>ขนาดหน้าจอของอุปกรณ์</li>
                <li>ประเทศที่ท่านอยู่</li>
                <li>ช่วงเวลาและระยะเวลาการเข้าใช้งานเว็บไซต์</li>
                <li>ประเภทบราวเซอร์</li>
                เราใช้ Google Analytics ในการเก็บและวิเคราะห์ข้อมูลเกี่ยวกับการใช้งาน  ท่านสามารถเรียนรู้เพิ่มเติมเกี่ยวกับ กูเกิลอนาไลติกส์ได้ที่ https://policies.google.com/privacy?hl=en
              </p>
              <h2 className="yellowText">
                วิธีการใช้งานข้อมูลส่วนบุคคล
              </h2>
              <p className="whiteText">
                เราใช้ข้อมูลของท่านเพื่อวิเคราะห์และนำเสนอสิ่งที่เป็นประโยชน์ และตรงตามความต้องการของท่านผู้ใช้งาน
              </p>
              <h2 className="yellowText">
                วิธีการติดต่อเรา
              </h2>
              <p className="whiteText">
                หากท่านมีข้อสงสัยหรือพบว่าเนื้อหาส่วนใดของนโยบายที่กล่าวมานี้เข้าใจได้ยาก หรือพบข้อบกพร่องอันเป็นอันตรายต่อการปกป้องสิทธิความเป็นส่วนตัวของท่าน สามารถติดต่อได้ที่ ที่อยู่อีเมล์ excbba@rumail.ru.ac.th
              </p>
            </div>
          </div>

          <div className="admission-box" id="transfer-details">
            <div id="admission-box-2" className="centered-box">
              <h2 className="yellowText">
                อยากเป็น..{' '}<span id="steptextLoopReview" />
                <TextLoop
                  interval={2500}
                  springConfig={{stiffness: 180, damping: 8}}
                  children={[
                    'นักวิเคราะห์นโยบายและแผน',
                    'นักวิชาการบริหารงานทั่วไป',
                    'นักวิชาการการเงิน',
                    'นักวางแผนการผลิต',
                    'งานบริหารคลังสินค้า',
                    'งานด้านการตลาด',
                    'พนักงานธนาคาร',
                    'งานเลขานุการ',
                  ]}
                />
              </h2>
              <div id="applyNowBtnReview">
                <Link to="/" className="blueText whiteBtnBlue">
                  กลับ
                </Link>
              </div>
            </div>
          </div>

        </div>
      </div>
    );
  }
}

export default Review;
