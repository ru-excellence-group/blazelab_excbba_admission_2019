import React, {Component} from 'react';
//import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import RUHeader from '../img/ru-header.png';
import ArrowDown from '../img/arrowdown.png';
import Model from '../img/model.png';
import AnchorLink from 'react-anchor-link-smooth-scroll';

class Introduction extends Component {
  render () {
    return (
      <div className="FullScreenBox" id="Introduction">
        <div className="WrapperFluid BlueBG WrapperFluidTop">
          <div className="centeredContent">
            <img id="ru-header-logo" src={RUHeader} alt="RU EXCBBA" />
            <h1 className="yellowText" id="header-text">
              โครงการพิเศษบริหารธุรกิจบัณฑิต<br />เพื่อความเป็นเลิศ
            </h1>
            <h1 className="whiteText noMargin" id="header-text-2">
              รับสมัครนักศึกษาใหม่
            </h1>
            <h5 className="whiteText noMargin" id="header-text-3">
              วันนี้ - 24 มิถุนายน 2562
            </h5>
          </div>
          <div className="introduction-bg-wrapper">
            <img id="introduction-bg" src={Model} alt="RU EXCBBA" />
          </div>
          <AnchorLink className="nextButton" href="#Review">
            <img id="ArrowDownButtonImg" src={ArrowDown} alt="ถัดไป" />
          </AnchorLink>
        </div>
      </div>
    );
  }
}

export default Introduction;
