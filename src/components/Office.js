import React, {Component} from 'react';
//import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
//import AnchorLink from 'react-anchor-link-smooth-scroll';

class Office extends Component {
  render () {
    return (
      <div className="FullScreenBoxNormal" id="OfficeBlock">
        <div className="WrapperNormal">

          <div className="admission-box" id="office">
            <div id="admission-box1" className="centered-box">
              <h2 className="blueText">สำนักงานโครงการฯ</h2>
              <ul className=" noStyle">
                <li>
                  ชั้น 2 คณะบริหารธุรกิจ มหาวิทยาลัยรามคำแหง หัวหมาก<br />
                  <br />
                  โทร 081-831-1399, 083-627-2439, 02-310-8213, 081-423-5069
                  <br />
                  เปิดทำการทุกวัน เว้นวันหยุดนักขัตฤกษ์<br />
                  เวลา 09.00 - 16.00 น.
                </li>
              </ul>
            </div>
          </div>

        </div>
      </div>
    );
  }
}

export default Office;
