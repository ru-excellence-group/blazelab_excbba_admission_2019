import React, {Component} from 'react';
import './App.css';
import {BrowserRouter as Router} from 'react-router-dom';
import AnchorLink from 'react-anchor-link-smooth-scroll';

import Introduction from './components/Introduction';
//import VideoIntro from './components/VideoIntro';
import Review from './components/Review';
import Admission from './components/Admission';
import Schedule from './components/Schedule';
import DownloadDoc from './components/DownloadDoc';
import Office from './components/Office';

import ArrowDown from './img/arrowdown.png';

class App extends Component {
  constructor (props) {
    super (props);
    this.state = {
      gototop: '',
    };
  }

  componentDidMount () {
    window.addEventListener ('scroll', this.listenToScroll);
  }

  componentWillUnmount () {
    window.removeEventListener ('scroll', this.listenToScroll);
  }
  listenToScroll = () => {
    const winScroll =
      document.body.scrollTop || document.documentElement.scrollTop;

    const height =
      document.documentElement.scrollHeight -
      document.documentElement.clientHeight;

    const scrolled = winScroll / height;

    if (scrolled > 0.1) {
      this.setState ({
        gototop: true,
      });
    } else {
      this.setState ({
        gototop: false,
      });
    }
  };

  render () {
    let Gotopstate = '';
    if (this.state.gototop) {
      Gotopstate += 'active';
    }
    return (
      <Router basename={'/admission'}>
        <div className="App">
          <header className="App-header" />

          <Introduction />
          <Review />
          <Admission />
          <Schedule />
          <DownloadDoc />

          <div id="sharebtngroup">
            <div
              className="fb-share-button"
              data-href="http://www.excbba.ru.ac.th/admission/"
              data-layout="button_count"
              data-size="large"
            >
              <a
                target="_blank"
                href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.excbba.ru.ac.th%2Fadmission%2F&amp;src=sdkpreparse"
                className="fb-xfbml-parse-ignore"
              >
                Share
              </a>
            </div>
            <a
              class="twitter-share-button my-twitter-share-button"
              href="https://twitter.com/intent/tweet?text=มาเรียนกับเราสิ%20โครงการพิเศษบริหารธุรกิจบัณฑิต%20เพื่อความเป็นเลิศ%20เปิดรับสมัครแล้ววันนี้%20รายละเอียดเพิ่มเติมที่เว็บไซต์&url=http://www.excbba.ru.ac.th/admission"
              target="_blank"
              data-size="large"
            >
              <svg
                className="twitter-logo-btn"
                xmlns="http://www.w3.org/2000/svg"
                width="18"
                height="18"
                viewBox="0 0 24 24"
              >
                <path d="M24 4.557c-.883.392-1.832.656-2.828.775 1.017-.609 1.798-1.574 2.165-2.724-.951.564-2.005.974-3.127 1.195-.897-.957-2.178-1.555-3.594-1.555-3.179 0-5.515 2.966-4.797 6.045-4.091-.205-7.719-2.165-10.148-5.144-1.29 2.213-.669 5.108 1.523 6.574-.806-.026-1.566-.247-2.229-.616-.054 2.281 1.581 4.415 3.949 4.89-.693.188-1.452.232-2.224.084.626 1.956 2.444 3.379 4.6 3.419-2.07 1.623-4.678 2.348-7.29 2.04 2.179 1.397 4.768 2.212 7.548 2.212 9.142 0 14.307-7.721 13.995-14.646.962-.695 1.797-1.562 2.457-2.549z" />
              </svg>
              Tweet
            </a>
          </div>

          <Office />

          {/* <div className="centered-box">กำลังอัพเดทเพิ่มเติม</div> */}

          {/* <Route path={`${process.env.PUBLIC_URL}/`} component={Home} />
          <Route path={`${process.env.PUBLIC_URL}/news`} component={News} />
          <Route path={`${process.env.PUBLIC_URL}/about`} component={About} /> 

          <a
              href="http://www.excmba.ru.ac.th"
              target="_blank"
              rel="noopener noreferrer"
            >
              Admission
            </a>
          
          */}

          <AnchorLink id="gototop" className={Gotopstate} href="#Introduction">
            <img src={ArrowDown} alt="Go to top" />
            {this.state.gototop}
          </AnchorLink>

        </div>
      </Router>
    );
  }
}

export default App;
